# run simulation to determine margin based on category
runs = 100000
resampling<-matrix(0,runs,10)

for(i in 1:nrow(Sum_pred_cat)){
  mean = as.numeric(Sum_pred_cat[i,2])
  sd = as.numeric(Sum_pred_cat[i,3])
  sim<- rnorm(runs, mean = mean, sd = sd)
  # Create a summary vector
  resampling[,i] <- sim
}

# for a value in pred_cat select and average 100 random rows in resample
col <- ncol(score_data_lean)
score_data_lean$margin_est_rand<-NA
prediction = as.data.frame(as.numeric(score_data_lean$pred_cat_factor))
for (j in 1:nrow(score_data_lean)){
  num<-as.numeric(score_data_lean[j,22])
  sample<- sample(resampling[,num], 10000, replace=FALSE)
  mean_margin <- round(mean(sample),0)
  score_data_lean[j,24]<- mean_margin
}

#create simulation of margin estimate
margin_sim <- score_data_lean %>% 
  filter(Margin == 999) %>% 
  select(team, opponent, pred_loss_prob, pred_win_prob, margin_est_linear, pred_cat_factor, status)

margin_sim<- merge(x = fixture, y = margin_sim, by = c("team", "opponent"))
margin_sim <- arrange(margin_sim, game)
games = nrow(fixture)
#fill matrix with columns representing the simulated game margins
# Look at including home or away match into the loop.
score_sim <- matrix(0, nrow = 10000, ncol = games)
for (k in 1:nrow(margin_sim)){
  num<-as.numeric(margin_sim[k,14])
  sample<- sample(resampling[,num], 10000, replace=FALSE)
  score_sim[,k] <- sample
}
score_sim <- as.data.frame(score_sim)
score_sim<-reshape2::melt(score_sim)
#get data frame of match data to merge with simulation
names <- data.frame(team=margin_sim$team, opp=margin_sim$opponent, game=rep(1:(games/2), each = 2), round = margin_sim$round, status = rep(1:2))
names<-as.data.frame(lapply(names, rep, 10000))
names <- arrange(names, game, status)
#merge names with score simulation
score_sim <- cbind(score_sim, names)
score_sim$match <- paste(score_sim$team, score_sim$opp, sep = " v ")
score_sim$order <- with(score_sim, match(match, unique(match)))

mean_score = score_sim %>%
  group_by(game, team, opp, order) %>% 
  summarise(rating.mean=mean(value), rating.sd = sd(value), rating.median = median(value))

mean_score = mean_score %>% 
  ungroup() %>% 
  mutate(result = ifelse(rating.mean < 0, opp, team),
         match = paste(team, opp, sep = " v "),
         status = rep(1:2, times = (games/2))) %>% 
  arrange(status, order)

#create custom colors for teams
cols <- c("Adelaide Thunderbirds" = '#ff6bd7',"GIANTS Netball" = '#f77d27',"Magpies Netball" = '#000000', "Melbourne Vixens" = '#008080',"NSW Swifts"= "#f72727", "Queensland Firebirds" ="#750b96","Sunshine Coast Lightning" = "#ffd456", "West Coast Fever" = "#1c960c", "Central Pulse" = "#ebe013", "Tactix" = "#e61a0b")

# create plot for simulation
plt<-score_sim %>% 
  mutate(result = ifelse(value < 0, opp, team)) %>% 
  filter(game < (games/2+1) , status == 1) %>% 
ggplot(aes(x = value, color = result))+
  geom_histogram(binwidth = 1, alpha = 0.9)+
  geom_vline(data=mean_score[1:(games/2),], aes(xintercept=rating.mean,  colour=result),
             linetype="dashed", size=1)+
  scale_colour_manual(values = cols)+
  labs(title = "Match simulation of Super Netball",
       subtitle = paste("Round", score_sim$round, sep = " "),
       color = "Team",
       x = "simulated margin")+
  scale_x_continuous(breaks = seq(-25, 25, 5))+
  scale_y_continuous(limits = c(0, 500)) +
  theme_netball()+
  facet_grid(game+match ~.,labeller = label_wrap_gen(width = 2, multi_line = TRUE))

#summary table to simulation results
tapply(score_sim$value, score_sim$match, summary)


library(ggrepel)
# add actual results actual to plot
matches_names<- mean_score[1:4,]
ann_text <- data.frame(value = c(0, -13, -2, 11), count = c(360, 50, 400, 300), 
                       result = c("West Coast Fever", "Queensland Firebirds", "Sunshine Coast Lightning", "Adelaide Thunderbirds"),
                       game =  c(1, 2, 3, 4), 
                       match = unique(matches_names$match))

# add actual results to data frame
mean_sim<-left_join(mean_score, ann_text, by = c("match", "game"))
mean_sim <- mean_sim[1:4,]
mean_sim <- mean_sim %>% 
  mutate(z_score = (value - rating.mean)/rating.sd)
mean_sim <- mean_sim %>%
  select(match, result.y, value, z_score) %>% 
  rename(winner = result.y, margin = value) %>% 
  mutate(z_score = round(z_score, digits = 2))

result_plot<-plt + geom_label_repel(data = ann_text, aes(x = value, y = count, label = result), 
                       nudge_x = 0,
                       box.padding = 0.5,
                       nudge_y = 350,
                       size = 5,
                       arrow = arrow(length = unit(0.1, "npc")))


#create data frame for visualization
margin_data <- score_data_lean %>%
  select(Margin,margin_est_linear, margin_est_rand, pred_win_prob, pred_cat, pred_cat_factor, status)

my.formula = y ~ x 
margin_data %>%
  filter(Margin != 999) %>% 
ggplot(aes(x = margin_est_rand, y = Margin, color = pred_cat)) +
  geom_point()+
  geom_smooth(method = "lm", se=TRUE, color="blue", formula = my.formula) +
  labs(color="Win Probability") +
  stat_poly_eq(formula = my.formula, 
               aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), 
               parse = TRUE)

margin_data %>%
  filter(Margin != 999) %>% 
  ggplot(aes(x = margin_est_linear, y = Margin, color = pred_cat)) +
  geom_point()+
  geom_smooth(method = "lm", se=TRUE, color="blue", formula = my.formula)+
  labs(color="Win Probability") +
  ggtitle("Predicted margin vs actual margin by wining prediction probability")+
  xlab("predicted margin")+
  ylab("actual margin")
  #stat_poly_eq(formula = my.formula, 
  #             aes(label = paste(..rr.label.., sep = "~~~")),label.y.npc = "bottom", 
  #             parse = TRUE)  

score_data_lean_error<-score_data_lean %>%
  filter(Margin != 999) %>% 
  mutate(margin_linear_error = Margin - margin_est_linear) %>% 
  mutate(margin_rand_error = Margin - margin_est_rand)

# Function that returns Root Mean Squared Error
rmse <- function(error)
{
  sqrt(mean(error^2))
}

# Function that returns Mean Absolute Error
mae <- function(error)
{
  mean(abs(error))
}

rmse(score_data_lean_error$margin_linear_error)
mae(score_data_lean_error$margin_linear_error)

rmse(score_data_lean_error$margin_rand_error)
mae(score_data_lean_error$margin_rand_error) 




