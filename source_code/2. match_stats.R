library(ggforce)
library(ggpmisc)

# Read in all json files
filenames <- list.files("C:/Users/mitch/Documents/R/netball_data/National/", pattern="*.json", full.names=TRUE) # this should give you a character vector, with each file name represented by an entry
myJSON <- lapply(filenames, function(x) fromJSON(file=x)) # a list in which each element is one of your original JSON files
match_stats_df<- data.frame(matrix (ncol = 0, nrow =0))
#run netball_match_function.R before running this line
for (j in myJSON) {
data <- netball_match(j)
#players <- players_onehotencode(j)
#data <- dplyr::left_join(data, players, by = 'matchId')
match_stats_df <- dplyr::bind_rows(match_stats_df, data) #%>% dplyr::mutate_if(is.numeric, ~replace(., is.na(.), 0))
#match_stats <-plyr::rbind.fill(match_stats, data) 
}
match_stats <- match_statistics(match_stats_df) # clean and organize data

# merge Glicko ratings and score dataframes based on week && team
# Filter only ratings
ratings_df <- plot_df %>%
  filter(var == "Rating")
#join the datasets from week and team
match_data <- left_join(match_stats, ratings_df,
                   by = c("week" = "week", "team" = "team"))

match_data$year <- as.integer(match_data$year)
match_data$points[match_data$points == 0] <- NA
#identify opposition rating
match_data<-match_data %>% 
  group_by(matchId) %>% 
  mutate(points = ifelse(is.na(points), goals, points)) %>% 
  mutate(Margin = (points*2) - (sum(points))) %>% 
  mutate(opp_goals = points - Margin) %>% 
  mutate(shoot = goals/goalAttempts) %>% 
  mutate(opp_pen = sum(unique(penalties)) - (penalties)) %>% 
  mutate(pen_diff = penalties - opp_pen) %>% 
  ungroup()

match_data<-match_data[match_data$team != match_data$opponent, ]   
#match_data<-match_data[!is.na(match_data$shoot), ]
#match_data<-match_data[!duplicated(match_data[,c("matchType", "year", "round", "game", "venue", "value", "shoot")]),]
#turn score difference into an integer D = 2, W = 1, L = 0
match_data <- match_data %>% 
  mutate(results = ifelse(Margin < 0, 0, ifelse(Margin > 0, 1, 2)))
#bind fixture
fixture <- readr::read_csv('csv_files/fixture.csv')
fixture$date <- as.Date(fixture$date, format = "%d/%m/%Y")
#use same datetime format
fixture$date <- as.POSIXct(fixture$date)

# add players playing in the upcoming match to the data frame
#new_match_data <- fixtured_matches(match = 11391, # SSN, ANZ - 10575 for 2021
#                                   round = 1,
#                                   game = 1:4)
#
#fixture <- fixture %>% left_join(new_match_data, by = c('team', 'opponent'))

match_data <- dplyr::bind_rows(match_data, fixture)

#change categorical variables e.g. team names & home and away status to integer values
match_data$Team <- as.numeric(ordered(match_data$team, levels = c("Southern Steel", "Central Pulse","West Coast Fever","Melbourne Vixens",
                                                                  "Queensland Firebirds","Adelaide Thunderbirds","Waikato BOP Magic",
                                                                  "Northern Mystics","Tactix","NSW Swifts","Northern Stars","GIANTS Netball",
                                                                  "Magpies Netball", "Sunshine Coast Lightning")))

match_data$opposition <- as.numeric(ordered(match_data$opponent, levels = c("Southern Steel", "Central Pulse","West Coast Fever","Melbourne Vixens",
                                                                            "Queensland Firebirds","Adelaide Thunderbirds","Waikato BOP Magic",
                                                                            "Northern Mystics","Tactix","NSW Swifts","Northern Stars","GIANTS Netball",
                                                                            "Magpies Netball", "Sunshine Coast Lightning")))

match_data$matchID<-paste(formatC((match_data$Team * match_data$opposition), width=1, flag="0"), sep=".")#get unique number in a sequence

match_data$competition <- ifelse(match_data$year < 2017, "ANZ",
                                 ifelse(match_data$year >= 2017 & match_data$team %in% c("Southern Steel", "Central Pulse", "Waikato BOP Magic","Northern Mystics","Tactix","Northern Stars"),"ANZP","SSN"))

match_data$competition <- as.numeric(ordered(match_data$competition, levels = c("ANZ", "ANZP", "SSN")))
match_data$Venue<-as.numeric(factor(match_data$venue))

# previous encounter data
match_data<-match_data %>% 
  group_by(team, opponent) %>% 
  mutate(last_encounter_margin = lag(Margin, order_by = date)) %>% 
  mutate(last_encounter_acc = lag(shoot, order_by = date)) %>% 
  mutate(last_encounter_penDiff = lag(pen_diff, order_by=date)) %>% 
  ungroup()

match_data<-match_data %>%
  group_by(team) %>%
  mutate(last_scoreDiff = lag(Margin, order_by=date)) %>%
  mutate(last_result = lag(results, order_by=date)) %>%
  mutate(team_rating = lag(value, order_by=date))%>%
  ungroup()

match_data<-match_data %>% 
  group_by(matchType,year, round, game, date) %>% 
  mutate(opp_rating = (sum(team_rating)-team_rating)) %>% 
  mutate(status = as.factor(seq(1,2)))%>%
  mutate(Status = ifelse(status == 1, "Home", "Away")) %>% 
  ungroup()

match_data<- match_data %>% 
  mutate(ranking_diff = team_rating - opp_rating) %>% 
  group_by(team) %>% 
  mutate(last_rankdiff = lag(ranking_diff, order_by=date))

# determine how many wins had for the year
match_data <- match_data%>%
  group_by(year, team) %>% 
  arrange(date)%>%
  mutate(wins_this_season = cumsum(ifelse(results == 2, 0.5, results))) %>% 
  mutate(matches_won = lag(wins_this_season, order_by = date),
         matches_won = ifelse(is.na(matches_won), 0, matches_won)) %>% 
  ungroup()

match_data<-match_data %>% 
  group_by(matchType,year, round, game, date) %>% 
  mutate(opp_wins = (sum(matches_won)-matches_won)) %>% 
  ungroup()

#col_names <- colnames(match_stats)
#col_names <- col_names[grepl("^[[:digit:]]+$", col_names)]
#players.data <- match_data[, col_names]

Margin <- match_data %>%
  select(team, opponent, game, results, year, Team, opposition, status,Venue, 
         last_encounter_margin,last_encounter_acc,competition, 
         last_result, team_rating, opp_rating,matches_won, opp_wins, Margin) %>% 
  mutate(Margin = ifelse(is.na(Margin), 999, Margin))
#Margin <- cbind(Margin, players.data)

Margin<-Margin[complete.cases(Margin), ] #remove NAs from data frame
Margin<-Margin%>%
  filter(results != 2) #remove draws
#Create data frame for margin predictions used in DeepLearning_Margin.R

model_data <- match_data %>%
  select(results, Team, opposition, status, Venue,
         last_encounter_margin,last_encounter_acc,competition,
         last_result, team_rating, opp_rating,matches_won, opp_wins
  )
#model_data <- cbind(model_data, players.data)

model_data<-model_data[complete.cases(model_data), ] #remove NAs from data frame
model_data<-model_data%>%
  filter(results != 2) #remove draws ensure that the loss function is "binary_crossentropy", if you want to keep Draws change to "categorical_crossentropy"

