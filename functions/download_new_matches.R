# get updated data files
new_matches <- function(match = 10575,
                        round = 1,
                        game = 1:4,
                        comp = 'SSN',
                        year = 2021){
  
  library(superNetballR)
  library(jsonlite)
  library(dplyr)
  
  new_match <- list()
  for(i in game){
    game_new <- downloadMatch(match, round, i)
    exportJSON <- toJSON(game_new)
    write(exportJSON, paste0("C:/Users/mitch/Documents/R/netball_data/National/", comp,"_",year,"_round",round,"_game",i,".json"))
  }
}

